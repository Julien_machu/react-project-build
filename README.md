# React Project Build

Ces fichiers vont permettre d'automatiser l'affichage d'un projet React chez l'utilisateur, de façon simple 

# Vagrant solution (windows / linux)
Pour Vagrant, je l'ai utilisé à partir de Windows 10 pro,
La configuration nécessaire : 
- installer Vagrant
- installer VirtualBox version 5.2 (la nouvelle version : la 6 n'est pas encore compatible avec Vagrant)
- Désactiver Hyper-v afin d'utiliser VirtualBox (sur Windows 10 c'est automatiquement activé)

Les intructions :
- dans un dossier vide, copiez le fichier Vagrantfile
- ouvrez un terminal dans ce dossier (tapez "cmd" là où il y a le chemin de votre dossier pour ouvrir une invite de commande)
- lancez la commande "vagrant up"
- patientez entre 5 et 15 minutes (selon la connection, et la puissance de la machine)
- une fois terminé, vous pourrez aller sur votre navigateur pour lancer localhost:3000 et voir votre projet
- pour quitter la VM, vu comme j'ai préparé le Vagrantfile, vous devrez envoyer des signaux de fermeture à la VM comme suit :
positionnez vous sur l'invite de commande puis faites ctrl-c plusieurs fois
- pour se connecter à la VM, depuis ce dossier, dans l'invit de commande tapez "vagrant ssh", vous verrez alors quelque chose comme Vagrant@centos-react.
- une fois connecter à la VM, pour en sortir (sans la fermer): faites "exit"
- pour la fermer (sans la supprimer) faites "sudo shutdown +0"
- une fois sorti de la VM, vous souhaitez fermer la VM sans la supprimer, tapez dans l'invit de commande "vagrant halt"
- pour la redémarrer, il vous faudra refaire un "vagrant up"
- pour supprimer la VM (et l'espace qu'elle prend, soit environ 1GO), tapez "vagrant destroy -f"

# Docker solution (linux)
J'aurai pu également créer le script pour qu'il soit compatible sur Windows
Ma configuration sur cet exemple : je suis sur une VM centos 7, 2cpu, 4086 mo de ram
La configuration nécessaire :
- il faut avoir Docker installé
- avoir accès au sudo

Les instructions :
- dans un dossier vide, copier le fichier Dockerfile et le fichier script-react-docker
(ou juste copier le dossier docker)
- ouvrez un terminal dans ce dossier
- lancez le script bash en tapant cette commande : "sudo bash script-react-docker"
- patientez 2 minutes (oui c'est beaucoup plus rapide que Vagrant)
- vous pouvez ouvrir un navigateur sur votre machine hote et aller à l'adresse localhost:3000 pour voir votre projet
- pour voir le conteneur qui a été créé, tapez $ sudo docker ps -a
- pour supprimer le conteneur : taper $ sudo docker rm IDENTIFIANT_DE_VOTRE_CONTENEUR
- si vous devez stoper le conteneur au préalable : tapez $ sudo docker stop IDENTIFIANT_DE_VOTRE_CONTENEUR
- pour voir les images qui ont été téléchargé, taper $ sudo docker images
- pour supprimer une image : tapez $ sudo docker rmi IDENTIFIANT_DE_VOTRE_IMAGE
- si vous souhaitez supprimer tous les conteneurs : $ sudo docker rm $( sudo docker ps -aq )
(ça ne supprimera pas les conteneurs encore actifs cela dit)
- si vous souhaitez supprimer toutes les images : $ sudo docker rmi $( sudo docker images -q )

